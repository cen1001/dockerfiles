FROM ubuntu:20.04
COPY chemistry.list.2004 /etc/apt/sources.list.d/chemistry.list
COPY postgres.list /etc/apt/sources.list.d/chemistry.list
COPY chemistry.gpg /etc/apt/trusted.gpg.d/chemistry.gpg
COPY postgres.gpg /etc/apt/trusted.gpg.d/postgres.gpg
ENV TZ="Europe/London"
ARG DEBIAN_FRONTEND="noninteractive" 
RUN apt-get update && apt-get install -y git curl vim wget perl make postgresql-client-13 python3-psycopg2 python3-dateutil python3-pip
RUN pip install black pre-commit flake8 isort
RUN adduser --disabled-password --uid 1000 --gecos "Catherine Pitt" cen1001

USER cen1001
WORKDIR /home/cen1001
COPY gitconfig /home/cen1001/.gitconfig
RUN mkdir -p /home/cen1001/.vim/pack/plugins/start
RUN git clone --depth=1 https://github.com/ervandew/supertab.git /home/cen1001/.vim/pack/plugins/start/supertab
RUN git clone --recursive https://github.com/davidhalter/jedi-vim.git /home/cen1001/.vim/pack/plugins/start/jedi-vim
RUN mkdir -p /home/cen1001/.vim/pack/python/start/black/plugin
RUN mkdir -p /home/cen1001/.vim/pack/python/start/black/autoload
RUN curl https://raw.githubusercontent.com/psf/black/stable/plugin/black.vim -o /home/cen1001/.vim/pack/python/start/black/plugin/black.vim
RUN curl https://raw.githubusercontent.com/psf/black/stable/autoload/black.vim -o /home/cen1001/.vim/pack/python/start/black/autoload/black.vim
RUN git clone https://github.com/lifepillar/pgsql.vim.git ~/.vim/pack/plugins/start/pgsql

RUN wget https://github.com/darold/pgFormatter/archive/refs/tags/v5.0.tar.gz
RUN tar -xzf /home/cen1001/v5.0.tar.gz
WORKDIR /home/cen1001/pgFormatter-5.0
RUN perl Makefile.PL
RUN make
USER root
RUN make install

USER cen1001
WORKDIR /home/cen1001
